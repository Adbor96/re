﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    private CharacterController controller;
    public float speed;
    public float defaultSpeed;
    [SerializeField] private float turnSmoothTime;
    [SerializeField] private float rayLength;
    float turnSmoothVelocity;
    //kameran som spelarens styrning går utifrån
    public Camera cam;

    private Transform groundCheck;
    private bool isGrounded;
    private float groundDistance = 0.4f;
    [SerializeField]
    private LayerMask groundMask;
    Vector3 velocity;

    private float gravity = -9.81f;

    private static CharacterMovement instance;
    public bool canInteract;
    // Start is called before the first frame update
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }
    void Start()
    {
        controller = GetComponent<CharacterController>();
        defaultSpeed = speed;
        groundCheck = transform.GetChild(2);
        canInteract = true;
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontal, 0, vertical).normalized;

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y <= 0)
        {
            velocity.y = -2;
        }
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.transform.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0, angle, 0);

            //spelarens riktning utinfrån kameran
            Vector3 moveDir = Quaternion.Euler(0, targetAngle, 0) * Vector3.forward;
            controller.Move(moveDir.normalized * speed * Time.deltaTime);
        }

        //spelaren ska kolla framför sig
        if (Input.GetKeyDown(KeyCode.E))
        {
            //skapa en ray som går framåt
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hitInfo;

            //visa strålen om den träffar
            if(Physics.Raycast(ray, out hitInfo, rayLength))
            {
                Interactable interactable = hitInfo.collider.GetComponent<Interactable>();
                if(interactable != null)
                {
                    Debug.DrawLine(ray.origin, hitInfo.point, Color.green);
                    //interactable.dialogManager.StartCoroutine(interactable.dialogManager.Type());
                    interactable.Interact();
                }
            }
            else
            {
                Debug.DrawLine(ray.origin, hitInfo.point, Color.red);
            }
            Debug.Log(hitInfo.collider);
        }
    }
    public void ActiveCamera(Camera activeCam)
    {
        cam = activeCam;
    }
}
