﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerFootSteps : MonoBehaviour
{
    private bool isGrounded;

    private Transform groundCheck;
    [SerializeField]
    private LayerMask groundMask;

    [SerializeField]
    private AudioClip[] stoneStepSounds;
    [SerializeField]
    private AudioClip[] rugStepSound;
    [SerializeField]
    private AudioClip[] woodStepSound;

    private float distanceMoved;
    [SerializeField]
    private float stepDistance;

    private string groundTag;

    private AudioSource source;
    private CharacterController controller;

    private bool gameIsRuning = false;

    private CharacterMovement movement;
    private Camera cam;
    //private List<AudioReverbZone> zones;
    private float zoneCount;

    CameraZones camZones;
    // Start is called before the first frame update
    void Start()
    {
        //zones = new List<AudioReverbZone>(); //detta måste användas
        groundCheck = transform.GetChild(2);
        source = GetComponent<AudioSource>();
        controller = GetComponent<CharacterController>();
        movement = GetComponent<CharacterMovement>();
        //cam = movement.cam;
        //AudioReverbZone[] _zones = cam.GetComponentsInChildren<AudioReverbZone>();
        /*for (int i = 0; i < _zones.Length; i++)
        {
            //zones.Add(_zones[i]);
        }*/

        gameIsRuning = true;
        //zoneCount = zones.Count;
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, 0.4f, groundMask);
        Collider[] ground = Physics.OverlapSphere(groundCheck.position, 0.4f, groundMask);

        if(isGrounded)
        {
            //Debug.Log("Ground " + ground[0]);
            groundTag = ground[0].gameObject.tag;
        }
        else
        {
            return;
        }


        if (controller.velocity.magnitude > 0) 
        {
            distanceMoved += Time.deltaTime;
            if (distanceMoved >= stepDistance)
            {
                    Step();

                    //gör detta för att kolla om reverb zonerna inte laddar tillräckligt snabbt för spelet
                    //NEJ, zonen för "stone" blir inte ens tillagd i listan
                    //Problemet kan vara att listan endast lägger till de zoner som är aktiva
                    //Det var det som var problemet, löst. (Se 137-142)

            }
        }
        
    }
    private AudioClip GetRandomClip()
    {
        /* for (int i = 0; i < zones.Count; i++)
         {
            zones[i].gameObject.SetActive(false);
         }*/
        AudioReverbZone[] zones = cam.GetComponentsInChildren<AudioReverbZone>();
        switch (groundTag)
        {
            case "Stone":
                source.pitch = Random.Range(0.8f, 1.2f);
                //zones[0].gameObject.SetActive(true);
                //camZones.zones[0].gameObject.SetActive(true);
                cam.GetComponent<CameraZones>().ChangeZone("Stone");
                return stoneStepSounds[Random.Range(0, stoneStepSounds.Length)];
            case "Rug":
                //zones[1].gameObject.SetActive(true);
                //camZones.zones[1].gameObject.SetActive(true);
                cam.GetComponent<CameraZones>().ChangeZone("Rug");
                return rugStepSound[Random.Range(0, rugStepSound.Length)];
            case "Wood":
                source.pitch = Random.Range(0.5f, 1.5f);
                return woodStepSound[Random.Range(0, woodStepSound.Length)];
            default:
                return null;
        }
    }
    void Step()
    {
        source.clip = GetRandomClip();
        source.Play();
        distanceMoved = 0;
    }
    private void OnDrawGizmos()
    {
        if (gameIsRuning)
        {
            Gizmos.DrawWireSphere(groundCheck.position, 0.4f);
        }
    }
    public void ChangeZone(string _preset)
    {
        //zones.Clear();
        //AudioReverbZone[] _zones = cam.GetComponentsInChildren<AudioReverbZone>();
        //for (int i = 0; i < _zones.Length; i++)
        //{
            //zones.Add(_zones[i]);
        //}


    }
    public void ActivateZones()
    {
        /*foreach (AudioReverbZone inactiveZone in zones)
        {
            inactiveZone.gameObject.SetActive(true);
            //Aktiverar alla zoner så att de kan läggas till i listan av zoner när kameran byts
        }*/
    }
    public void SetCam(Camera _cam)
    {
        cam = _cam;
    }
    public void SetZones(CameraZones _camZones)
    {
        camZones = _camZones;
    }
}
