﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialog : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    private string[] sentences;
    private int index;
    public float typingSpeed;
    private CharacterMovement player;
    private InteractableText[] InteractTextObjects;
    //om spelaren kan klicka till nästa bit dialog
    private bool canGoToNextDialog;
    //om en dialog faktiskt håller på att visas
    private bool inDialogue;

    private float lastTime;

    private void Start()
    {
        InteractTextObjects = FindObjectsOfType<InteractableText>();
        player = GameObject.FindObjectOfType<CharacterMovement>();
        lastTime = Time.realtimeSinceStartup;

        for (int i = 0; i < InteractTextObjects.Length; i++)
        {
            InteractTextObjects[i].OnInteractEvent += Dialog_OnInteractEvent;
        }
        //StartCoroutine(Type());
    }

    private void Dialog_OnInteractEvent(string[] _sentences, int _index)
    {
        sentences = _sentences;
        index = _index;
        StartCoroutine(Type());
    }

    private void Update()
    {//spelaren kan klicka vidare om den nuvarande meningen har visats helt
        if (inDialogue)
        {
            if (textDisplay.text == sentences[index])
            {
                canGoToNextDialog = true;
            }
            else
            {
                canGoToNextDialog = false;
            }
            if (canGoToNextDialog && Input.GetMouseButtonDown(0))
            {
                NextSentence();
            }
        }
    }
    public IEnumerator Type()
    {
        inDialogue = true;
        Time.timeScale = 0;
        textDisplay.text = "";
        //för varje bokstav i meningen, vänta en sekund med att lägga till nästa bokstav
        foreach (var letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSecondsRealtime(typingSpeed);
        }
    }
    //när spelaren klickar ska den gå till nästa del av dialogen
    private void NextSentence()
    {
        canGoToNextDialog = false;
        if(index < sentences.Length - 1)
        {
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
            
        }
        else
        {
            textDisplay.text = "";
            player.speed = player.defaultSpeed;
            player.canInteract = true;
            inDialogue = false;
            canGoToNextDialog = false;
            Time.timeScale = 1;
        }
    }
}
