﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : Interactable
{
    [SerializeField]
    private Animator sceneTransitionAnim;
    [SerializeField]
    private string nextScene;
    [SerializeField]
    private AudioClip[] doorSounds;
    private AudioSource source;
    private SpawnManager manager;
    [SerializeField]
    private Vector3 spawnPosition; //där spelaren ska spawna när nästa scen laddas
    CharacterMovement player;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        manager = FindObjectOfType<SpawnManager>();
        player = FindObjectOfType<CharacterMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if(manager == null)
        {
            manager = FindObjectOfType<SpawnManager>();
        }
    }
    public override void Interact()
    {
        if(manager != null)
        {
            Debug.Log("Change scene");
            StartCoroutine(ChangeScene());
        }
    }
    private IEnumerator ChangeScene()
    {
        manager.SetPosition(spawnPosition);
        source.clip = randomClip();
        source.Play();
        sceneTransitionAnim.SetTrigger("ChangeScene");
        yield return new WaitForSeconds(1);
        //player.gameObject.transform.position = spawnPosition;
        Debug.Log(spawnPosition);
        manager.NewScene();
        SceneManager.LoadScene(nextScene);
    }
    private AudioClip randomClip()
    {
        return doorSounds[Random.Range(0, doorSounds.Length)];
    }
}
