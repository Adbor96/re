﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnManager : MonoBehaviour
{
    private string SceneName; 
    private Vector3 spawnPosition;
    public static SpawnManager instance;
    CharacterMovement player;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this);
        player = FindObjectOfType<CharacterMovement>();
        //player.transform.position = spawnPosition;
    }
    // Start is called before the first frame update
    void Start()
    {
        SceneName = SceneManager.GetActiveScene().name;
    }

    // Update is called once per frame
    void Update()
    {
        if(SceneName != SceneManager.GetActiveScene().name)
        {
            SceneName = SceneManager.GetActiveScene().name;
            Debug.Log(SceneName);
            player.GetComponent<CharacterController>().enabled = true;
        }
    }
    public void SetPosition(Vector3 _spawnPos)
    {
        spawnPosition = _spawnPos;
    }
    public void NewScene()
    {
        player.GetComponent<CharacterController>().enabled = false;
        player.transform.position = spawnPosition;
    }
}
