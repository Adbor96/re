﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class WinePuzzle : MonoBehaviour
{
    private List<int> numbers;
    private bool puzzleSolved;
    [SerializeField]
    private AudioClip switchSound, solvedSound;
    AudioSource source;

    [SerializeField]
    private Puzzle currentPuzzle;
    // Start is called before the first frame update
    void Start()
    {
        numbers = new List<int>();
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(numbers.Count == 4)
        {
            if (numbers[0] == 1 && numbers[1] == 9 && numbers[2] == 4 && numbers[3] == 7)
            {
                PuzzleClear();
            }
            else
            {
                numbers.Clear();
            }
        }

    }
    public void AddNumber(int newNumber)
    {
        if (numbers.Count != 4)
        {
            numbers.Add(newNumber);
            source.clip = switchSound;
            source.Play();
            Debug.Log("Numbers list " + numbers.Count);
        }
        else
        {

        }
    }
    void PuzzleClear()
    {
        if (!puzzleSolved)
        {
            Debug.Log("Puzzle Clear");
            source.clip = solvedSound;
            source.Play();
            //StartCoroutine(ExitPuzzleOnClear());
            ForceExitPuzzle();
            puzzleSolved = true;
        }
    }
    IEnumerator ExitPuzzleOnClear()
    {
        Debug.Log("Exiting Puzzle");
        yield return new WaitForSeconds(3);
        currentPuzzle.ExitPuzzle();
    }
    void ForceExitPuzzle()
    {
        currentPuzzle.ExitPuzzle();
    }
}
