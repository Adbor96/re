﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : Interactable
{
    [SerializeField]
    private Camera puzzleCam;

    private Camera currentCam;
    CharacterMovement player;

    private bool move;
    private bool puzzleisSolved;
    private AudioSource source;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<CharacterMovement>();
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (puzzleisSolved)
        {
            if(transform.position.x <= 18.5f)
            {
                source.Play();
                transform.Translate(new Vector3(0, 0, -1) * Time.deltaTime);
            }
        }
    }
    public override void Interact()
    {
        currentCam = player.cam;
        puzzleCam.gameObject.SetActive(true);
        player.cam = puzzleCam;
        currentCam.gameObject.SetActive(false);
        Time.timeScale = 0;
    }
    public void ExitPuzzle()
    {
        currentCam.gameObject.SetActive(true);
        player.cam = currentCam;
        puzzleCam.gameObject.SetActive(false);
        Time.timeScale = 1;
        PuzzleSolved();
    }
    public void PuzzleSolved()
    {
        puzzleisSolved = true;
    }
}
