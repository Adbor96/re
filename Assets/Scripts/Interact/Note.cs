﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Note : Interactable
{
    [SerializeField]
    private Camera noteCamera;
    private Camera currentCam;

    CharacterMovement player;
    private AudioSource source;

    private bool isInteracting;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<CharacterMovement>();
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isInteracting)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                ExitNote();
            }
        }
    }
    public override void Interact()
    {
        if (!isInteracting)
        {
            source.Play();
            currentCam = player.cam;
            noteCamera.gameObject.SetActive(true);
            player.cam = noteCamera;
            currentCam.gameObject.SetActive(false);
            isInteracting = true;
            Time.timeScale = 0;
        }
    }
    void ExitNote()
    {
        source.Play();
        currentCam.gameObject.SetActive(true);
        player.cam = currentCam;
        noteCamera.gameObject.SetActive(false);
        Time.timeScale = 1;
        isInteracting = false;
    }
}
