﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InteractableText : Interactable
{
    public string[] sentences;
    private int index;
    private CharacterMovement player;
    private bool canInteract;

    public delegate void SendSentenceDelegate(string[] _sentences, int _index);
    public event SendSentenceDelegate OnInteractEvent;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindObjectOfType<CharacterMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void Interact()
    {
        if (player.canInteract)
        {
            //skicka ut hur många meningar och vad de säger till Dialog Manager
            OnInteractEvent?.Invoke(sentences, index);
            player.canInteract = false;
        }
    }
    public void ExitText()
    {
        canInteract = true;
    }
}
