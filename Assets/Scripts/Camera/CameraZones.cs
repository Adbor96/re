﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class CameraZones : MonoBehaviour
{
    CharacterMovement player;
    PlayerFootSteps _player;
    [SerializeField]
    private List<Zone> zones;
    AudioReverbZone _zone;
    // Start is called before the first frame update
    private void Awake()
    {
        player = FindObjectOfType<CharacterMovement>();
        _player = FindObjectOfType<PlayerFootSteps>();
        //zones = new List<AudioReverbZone>();
        _zone = GetComponentInChildren<AudioReverbZone>(); 
        player.ActiveCamera(GetComponent<Camera>());
        _player.SetCam(GetComponent<Camera>());
        _player.SetZones(this);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void ChangeZone(string _preset) //ändra audioreverbzone preset till den som passar
    {
        for (int i = 0; i < zones.Count; i++)
        {
            if(zones[i].zoneTag == _preset)
            {
                _zone.reverbPreset = zones[i].preset;
                return;
            }
        }
    }
}
