﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChangeTrigger : MonoBehaviour
{
    //den trigger som ska slås på när spelaren går in i och avaktiverar denna trigger
    [SerializeField] private GameObject triggerToChange;
    //den kamera som ska stängas av
    [SerializeField] private Camera cameraToTurnoff;
    //kameran som ska slås på
    [SerializeField] private Camera cameraToTurnOn;

    private PlayerFootSteps footSteps;
    // Start is called before the first frame update
    private void Awake()
    {
        footSteps = FindObjectOfType<PlayerFootSteps>();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        CharacterMovement player = other.GetComponent<CharacterMovement>();
        PlayerFootSteps _player = other.GetComponent<PlayerFootSteps>();
        if(player != null)
        {
            cameraToTurnoff.gameObject.SetActive(false);
            triggerToChange.SetActive(true);
            cameraToTurnOn.gameObject.SetActive(true);
            footSteps.ActivateZones(); //aktivera alla audiozones (de kan endast läggas till i listan av zones om de är aktiverade)
            player.ActiveCamera(cameraToTurnOn);
            _player.SetCam(cameraToTurnOn);
            _player.SetZones(cameraToTurnOn.GetComponent<CameraZones>());
            //footSteps.cam = cameraToTurnOn;
           // footSteps.ChangeZones();
            Debug.Log("active cam = " + player.cam);
            gameObject.SetActive(false);
        }
    }
}
