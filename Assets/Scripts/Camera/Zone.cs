﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Zone 
{
    public string zoneTag;
    public AudioReverbPreset preset;
}
