﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoEnd : MonoBehaviour
{
    [SerializeField]
    private GameObject demoOverScreen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        CharacterMovement player = other.GetComponent<CharacterMovement>();
        if(player != null)
        {
            demoOverScreen.SetActive(true);
            player.speed = 0;
        }
    }
}
